import {RootStore} from "../stores/RootStore";
import React from "react";

export const StoreContext = React.createContext(new RootStore());
export const StoreProvider = StoreContext.Provider;
export const useStores =  () => React.useContext(StoreContext);