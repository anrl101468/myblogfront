import axios from "axios";

export const instance = axios.create();

instance.interceptors.request.use(
    function (config) {
        config.withCredentials = true;
        const authToken = sessionStorage.getItem("Authorization");

        if(authToken){
            config.headers.Authorization = authToken;
        }
        /*console.log("config = " + JSON.stringify(config));*/
        return config;
    },
    function (error) {
        // 오류 요청을 보내기전 수행할 일
        // ...
        return Promise.reject(error);
    });