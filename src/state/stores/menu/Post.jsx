import {makeObservable, observable} from "mobx";

export default class Post
{
    uplIdx;
    uplTitle;
    uplContent;
    open=false;
    uplRegDt;
    constructor(uplIdx,uplTitle,uplContent,uplRegDt) {
        this.uplIdx = uplIdx;
        this.uplTitle = uplTitle;
        this.uplContent = uplContent;
        this.uplRegDt = uplRegDt;
        this.open = false;
        makeObservable(this,{
            uplIdx:observable,
            uplTitle:observable,
            uplContent:observable,
            open:observable,
        });
    }
}