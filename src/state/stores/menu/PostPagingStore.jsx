import {action, makeObservable, observable} from "mobx";

export default class PostPagingStore
{
    rootStore;

    page;

    rowsPerPage;
    constructor(root) {
        this.rootStore = root;
        this.page = 0;
        this.rowsPerPage = 5;

        makeObservable(this,{
            page:observable,
            rowsPerPage:observable,
            rowsPerPageChange:action,
            pageChange:action,
        })

    }

    rowsPerPageChange(value)
    {
        this.rowsPerPage = value;
        this.page = 0;
    }

    pageChange(value)
    {
        this.page = value;
    }
}
