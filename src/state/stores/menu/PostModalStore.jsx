import {action, makeObservable, observable} from "mobx";

export default class PostModalStore
{
    rootStore;

    title='';
    content = '';
    type ='';
    uplIdx = '';

    constructor(root) {
        this.rootStore = root;
        this.title ='';
        this.content ='';
        this.type = 'create';

        makeObservable(this,{
            title:observable,
            content:observable,
            type:observable,
            uplIdx:observable,
            titleChange:action,
            contentChange:action,
            typeChange:action,
            uplIdxChange:action
        })
    }

    titleChange(value)
    {
        this.title = value;
    }

    contentChange(value)
    {
        this.content = value;
    }

    typeChange(value)
    {
        this.type = value;
    }

    uplIdxChange(value)
    {
        this.uplIdx = value;
    }
}