import {action, makeObservable, observable} from "mobx";
import Post from "./Post";
import {instance} from "../../axiosConfig/AxiosConfig";
import qs from "qs";

export default class BlogStore
{
    rootStore;

    postWriteOpen;

    menuIdx;

    menuIndex;

    subMenuIndex;

    posts = [];

    subMenu;
    searchKeyword;
    keyboardArrowUpIconOpen = false;
    constructor(root) {
        this.rootStore = root;
        this.postWriteOpen = false;
        this.keyboardArrowUpIconOpen = false;
        this.menuIdx = null;
        this.searchKeyword = '';
        makeObservable(this,{
            menuIdx:observable,
            posts:observable,
            searchKeyword:observable,

            postWriteOpen:observable,
            keyboardArrowUpIconOpen:observable,

            PostWriteModalOpen: action,
            PostWriteModalClose: action,
            expandIconClick: action,

            setMenuIdx: action,
            setIndex: action,

            createPost : action,
            modifyPost : action,
            newPost : action,
            selectPost : action,

            searchKeywordChange:action,
        })
    }

    setMenuIdx(menuIdx)
    {
        this.menuIdx = menuIdx;

        if(!this.searchKeyword)
        {
            this.selectPost();
        }
    }

    setIndex(menuIndex,subMenuIndex)
    {
        this.menuIndex = menuIndex;
        this.subMenuIndex = subMenuIndex;

        const subMenu = this.rootStore.mainLeftRootMenuStore.leftMenus[this.menuIndex].subMenus[this.subMenuIndex];

        if(subMenu)
        {
            this.setMenuIdx(subMenu.menuIdx);
        }

        return subMenu;
    }

    PostWriteModalOpen()
    {
        this.postWriteOpen = true;
    }

    PostWriteModalClose()
    {
        this.postWriteOpen = false;
    }

    newPost(res)
    {
        this.posts = [];
        res.data.map( (x) => {
            this.posts.push(
                new Post(x.uplIdx,x.uplTitle,x.uplContent,x.uplRegDt)
            )
        })

    }

    async createPost(title, content) {
        const url = "/api/upl/uplCreate";
        const data = {
            uplTitle: title,
            uplContent: content,
            menuIdx: this.menuIdx,
        }

        const qsData = qs.stringify(data);
        await instance.post(url, qsData).then(res => {

            this.rootStore.postStore.titleChange("");
            this.rootStore.postStore.contentChange("");
            this.selectPost();
            this.PostWriteModalClose();
        });

    }

    async modifyPost(title, content, uplIdx) {
        const url = "/api/upl/uplUpdate";
        const data = {
            uplTitle: title,
            uplContent: content,
            uplIdx: uplIdx,
        }

        const qsData = qs.stringify(data);
        await instance.post(url, qsData).then(res => {

            this.rootStore.postStore.titleChange("");
            this.rootStore.postStore.contentChange("");
            this.selectPost();
            this.PostWriteModalClose();
        });
    }

    async deletePost(uplIdx) {
        const url = "/api/upl/uplDelete";
        const data = {
            uplIdx: uplIdx,
        }

        const qsData = qs.stringify(data);
        await instance.post(url, qsData).then(res => {

            this.rootStore.postStore.titleChange("");
            this.rootStore.postStore.contentChange("");
            this.selectPost();
            this.PostWriteModalClose();
        });
    }


    async selectPost(usrId) {
        const url = "/api/upl/uplSelect";

        var data;
        if(usrId)
        {
            data = {
                menuIdx: this.menuIdx,
                searchKeyword : this.searchKeyword,
                usrId : usrId
            }
        }
        else
        {
            data = {
                menuIdx: this.menuIdx,
                searchKeyword : this.searchKeyword,
            }
        }

        const qsData = qs.stringify(data);
        await instance.post(url, qsData)
            .then(res => {
                    this.newPost(res);
            });
    }

    expandIconClick(index)
    {
        this.posts[index].open = !this.posts[index].open;
    }

    searchKeywordChange(value)
    {
        this.searchKeyword = value;
        if(value.length >= 1)
        {
            const url = "/api/upl/uplSelect";
            const data = {
                menuIdx: this.menuIdx,
                searchKeyword : this.searchKeyword,
            }

            const qsData = qs.stringify(data);
            instance.post(url,qsData)
                .then( res => {
                   this.newPost(res);
                });
        }
    }
}