import {action, makeObservable, observable} from "mobx";
import qs from 'qs';
import {instance} from "../../axiosConfig/AxiosConfig";
class JoinStore
{
    rootStore;
    usrIdx ="";
    usrId = "";
    usrName = "";
    usrPassword = "";



    constructor(root)
    {
        this.rootStore = root;

        makeObservable(this,{
            usrIdx:observable,
            usrId:observable,
            usrName:observable,
            usrPassword:observable,
            joinAxios:action,
            usrIdOnChange:action,
            usrNameOnChange:action,
            usrPasswordOnChange:action,
        })

    }

    usrIdOnChange = (usrId) =>
    {
        this.usrId = usrId;
    }

    usrNameOnChange = (usrName) =>
    {
        this.usrName = usrName;
    }

    usrPasswordOnChange = (usrPassword) =>
    {
        this.usrPassword = usrPassword;
    }

    joinAxios = (navigate) =>
    {
        const joinUrl = "/api/joinProc"
        const usr = {
            usrId : this.usrId,
            usrPassword : this.usrPassword,
            usrName : this.usrName,
        }
        const qsUsr = qs.stringify(usr);
        instance.post(joinUrl, qsUsr
        ).then(res => {
            this.rootStore.authorizationStore.setAuthorization(res.headers.get("Authorization"));
            this.rootStore.usrStore.setUsr(usr);
            navigate("/joinComplete");
        })
        .catch( (error) => {
            throw new Error(error);
        })
    }



}

export default JoinStore;