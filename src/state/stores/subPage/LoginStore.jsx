import {action, makeObservable, observable} from "mobx";
import qs from "qs";
import {instance} from "../../axiosConfig/AxiosConfig";

export class LoginStore
{
    rootStore;

    usrId = '';
    usrPassword = '';
    message = '';

    constructor(root) {
        this.rootStore = root;

        if(localStorage.getItem("usrId"))
        {
            this.usrId = localStorage.getItem("usrId");
        }
        makeObservable(this,{
            usrId:observable,
            usrPassword:observable,
            message : observable,
            usrIdChange:action,
            usrPasswordChange:action,
            logout:action,
            setMessage:action,
        });
    }

    setMessage(message)
    {
        this.message = message;
    }
    usrIdChange(usrId)
    {
        this.usrId = usrId;
    }

    usrPasswordChange(usrPassword)
    {
        this.usrPassword = usrPassword;
    }

    loginClick = (navigate) =>
    {
        const url = "/api/login"
        const usr =
            qs.stringify({
                usrId : this.usrId,
                usrPassword : this.usrPassword
            })
        instance.post(url,usr)
            .then(res => {
                if(res.headers.get("Authorization"))
                {
                    localStorage.setItem("usrId",this.usrId);
                    this.rootStore.authorizationStore.setAuthorization(res.headers.get("Authorization"))
                    console.log("로그인의 성공하셧습니다.");
                    this.rootStore.mainLeftRootMenuStore.getMainMenuServerInfo();
                    this.rootStore.usrStore.getServerUserInfo();
                    navigate("/");
                }
                else
                {
                    this.setMessage(decodeURIComponent(res.headers.get("loginFail")).replace(/\+/g, ' '));
                }
            })
            .catch( error => {
                throw new Error(error);
            })
    }

    logout = (navigate) =>
    {
        const url = "/api/logout"

        instance.post(url)
            .then(res => {
                if(res.status === 200)
                {
                    this.rootStore.authorizationStore.setAuthorization(null);
                    this.rootStore.usrStore.setUsr(null);
                    this.rootStore.mainLeftRootMenuStore.reSetLeftMenus();
                    navigate("/")
                    console.log("로그아웃의 성공했습니다.");
                }
            }).catch(error => {
                console.error(error);
        })
    }
}