import {action, makeObservable, observable} from "mobx";
import {instance} from "../../axiosConfig/AxiosConfig";

export class UsrStore
{
    rootStore;

    usr;
    constructor(root) {
        this.rootStore = root;
        this.usr = null;
        const token = sessionStorage.getItem("Authorization");
        if(token){
            this.getServerUserInfo();
        }

        makeObservable(this,{
            usr:observable,
            setUsr:action,
            getServerUserInfo:action,
        })
    }

    setUsr(usr)
    {
        this.usr = usr;
    }

    getServerUserInfo()
    {
        const url = "/api/usr/getUsrInfo";
        const data = sessionStorage.getItem("Authorization");
        instance.post(url,data)
            .then(res => {
                this.setUsr(res.data);
            }).catch(error => {
                console.error(error);
        })
    }
}