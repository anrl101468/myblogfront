import MainLeftRootMenuStore from "./mainPage/leftMenu/MainLeftRootMenuStore";
import {MainLeftMenuModalStore} from "./mainPage/leftMenu/MainLeftMenuModalStore";
import {LeftMenu} from "./mainPage/leftMenu/LeftMenuStore";
import {AlertDialogStore} from "./mainPage/leftMenu/AlertDialogStore";
import JoinStore from "./subPage/JoinStore";
import {UsrStore} from "./usr/UsrStore";
import {LoginStore} from "./subPage/LoginStore";
import {AuthorizationStore} from "./AuthorizationStore";
import BlogStore from "./menu/BlogStore";
import PostModalStore from "./menu/PostModalStore";
import PostPagingStore from "./menu/PostPagingStore";

export class RootStore
{
    mainLeftRootMenuStore;
    mainLeftMenuModalStore;
    leftMenuStore;
    alertDialogStore;
    joinStore;
    usrStore;
    loginStore;
    authorizationStore;
    blogStore;
    postStore;
    postPagingStore;
    constructor() {
        this.mainLeftRootMenuStore = new MainLeftRootMenuStore(this);
        this.mainLeftMenuModalStore = new MainLeftMenuModalStore(this);
        this.leftMenuStore = new LeftMenu();
        this.alertDialogStore = new AlertDialogStore(this);
        this.joinStore = new JoinStore(this);
        this.usrStore = new UsrStore(this);
        this.loginStore = new LoginStore(this);
        this.authorizationStore = new AuthorizationStore(this);
        this.blogStore = new BlogStore(this);
        this.postStore = new PostModalStore(this);
        this.postPagingStore = new PostPagingStore(this);
    }
}