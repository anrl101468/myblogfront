import {action, makeObservable, observable, runInAction} from "mobx";
import {SubMenuStore} from "./SubMenuStore";
import qs from "qs";
import {instance} from "../../../axiosConfig/AxiosConfig";

export class LeftMenu
{
    buttonName;
    buttonId;
    menuId;
    open;
    anchorEl;
    menuIdx;
    subMenus;
    menuUpIdx;

    constructor(menuName, buttonId, menuId, open, anchorEl, menuIdx, menuUpIdx) {

        this.buttonName = menuName;
        this.buttonId = buttonId;
        this.menuId = menuId;
        this.open = open || false;
        this.anchorEl = anchorEl || null;
        this.menuIdx = menuIdx;
        this.menuUpIdx = menuUpIdx;
        this.subMenus = [];

        this.getSubMenuServerInfo();

        makeObservable(this, {
            buttonName: observable,
            buttonId: observable,
            menuId: observable,
            open: observable,
            anchorEl: observable,
            menuIdx:observable,
            menuUpIdx:observable,
            subMenus: observable,
            createSubMenu : action,
            deleteSubMenu : action,
            getSubMenuServerInfo : action,
        });

    }
    async createSubMenu(subMenuName) {
        const url = "/api/menu/menuCreate";
        const data = {
            menuUpIdx: this.menuIdx,
            menuName: subMenuName
        }

        const qsData = qs.stringify(data);
        const res = await instance.post(url, qsData);
        runInAction( () => {
            this.subMenus = [
                ...this.subMenus,
                new SubMenuStore(res.data.menuUpIdx, res.data.menuIdx, res.data.menuName)
            ];
        });
    }

    async deleteSubMenu(index) {
        const url = "/api/menu/menuDelete";
        const data = {
            menuIdx: this.subMenus[index].menuIdx,
        }
        const qsData = qs.stringify(data);
        await instance.post(url, qsData)
            .then(res => {
                if (res.status === 200) {
                    console.log("정상적으로 삭제되었습니다.");
                }
            }).catch(error => {
                console.error(error);
            })
        await this.getSubMenuServerInfo();
    }

    getSubMenuServerInfo = async () => {
        const url = "/api/menu/menuSelect";
        const data = {
            menuUpIdx: this.menuIdx
        }
        const qsData = qs.stringify(data);
        const res = await instance.post(url, qsData);
        runInAction(() => {
            const subMenus = [];
            res.data.forEach((data) => {
                subMenus.push(new SubMenuStore(data.menuUpIdx, data.menuIdx, data.menuName));
            });
            this.subMenus = subMenus;
        });
    }

}