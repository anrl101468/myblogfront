import {action, makeObservable, observable} from "mobx";

export class AlertDialogStore
{
    rootStore;

    open = false;
    target ='';
    index = 0;
    constructor(root) {
        this.rootStore = root;
        this.open = false;
        this.target ='';
        this.index = 0;
        makeObservable(this,{
            open:observable,
            target:observable,
            index:observable,
            handleClickOpen:action,
            handleClose:action,
            setTarget:action,
            clearTarget:action,
            setIndex:action,
        });
    }

    setTarget = (target) =>
    {
        this.target = target;
    }

    clearTarget = () =>
    {
        this.target = '';
    }

    setIndex = (index) =>
    {
        this.index = index;
    }
    handleClickOpen = () => {
        this.open = true;
    };

    handleClose = () => {
        this.open=false;
    };
}