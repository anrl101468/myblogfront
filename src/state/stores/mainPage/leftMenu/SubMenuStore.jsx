import {makeObservable, observable} from "mobx";

export class SubMenuStore
{
    subMenuName = '';
    menuIdx = '';
    menuUpIdx = '';
    constructor(menuUpIdx,menuIdx,subMenuName) {
        this.menuUpIdx = menuUpIdx;
        this.menuIdx = menuIdx;
        this.subMenuName = subMenuName;
        makeObservable(this,{
            subMenuName:observable,
            menuIdx:observable,
            menuUpIdx:observable
        })
    }
}