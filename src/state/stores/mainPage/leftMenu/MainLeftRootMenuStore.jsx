import {action, makeObservable, observable, runInAction} from "mobx";
import {LeftMenu} from "./LeftMenuStore";
import qs from "qs";
import {instance} from "../../../axiosConfig/AxiosConfig";


class MainLeftRootMenuStore {

    rootStore;

    leftMenus = [];

    showIcon = false;
    constructor(root) {
        this.rootStore = root;
        this.reRenderLing();

        makeObservable(this,{
            leftMenus : observable,
            showIcon : observable,
            handleOpen : action,
            handleClose : action,
            createLeftMenu : action,
            deleteLeftMenu : action,
            createSubMenu : action,
            deleteSubMenu : action,
            getMainMenuServerInfo : action,
            reRenderLing : action,
            reSetLeftMenus : action,
            showIconChange : action,
        });


    }

    showIconChange = () => {
        this.showIcon = ! this.showIcon;
    }

    reSetLeftMenus = () => {
        this.leftMenus = [];
        sessionStorage.removeItem("leftMenus");
    }
    createLeftMenu = async (menuName) => {
        const url = "/api/menu/menuCreate"
        const data = {
            menuName: menuName
        }
        const qsData = qs.stringify(data);

        await instance.post(url, qsData)
            .then(res => {
                if (res.status === 200) {
                    this.getMainMenuServerInfo();
                }
            }).catch(error => {
                console.log(error);
            })
    }

    modifyLeftMenu = async (menuName,menuIdx) => {
        const url = "/api/menu/menuUpdate"
        const data = {
            menuName : menuName,
            menuIdx : menuIdx,
        }
        const qsData = qs.stringify(data);

        await instance.post(url, qsData)
            .then(res => {
                if (res.data) {
                    this.getMainMenuServerInfo();
                }
                else
                {
                    console.log("메뉴이름 업데이트에 실패하였습니다.");
                }
            }).catch(error => {
                console.log(error);
            })
    }

    createSubMenu = (index,subName) =>
    {
        this.leftMenus[index].createSubMenu(subName);
    }

    deleteSubMenu = (index) =>
    {
        this.leftMenus[index].deleteSubMenu(index);
    }

    deleteLeftMenu = async (index) => {
        const url = "/api/menu/menuDelete"
        const data = {
            menuIdx: this.leftMenus[index].menuIdx,
            menuUpIdx: this.leftMenus[index].menuIdx
        }

        const qsData = qs.stringify(data);
        await instance.post(url, qsData)
            .then( res => {
                console.log(res);
            }).catch(error => {
                console.error(error);
            })
        await this.getMainMenuServerInfo();
    }


    handleOpen = (event,index) =>
    {
        this.leftMenus[index].anchorEl = event.currentTarget;
        this.leftMenus[index].open=!this.leftMenus[index]._open;
    }

    handleClose = (event,index) => {
        this.leftMenus[index].open=false;
    }

    getMainMenuServerInfo = async (usrId) => {
        const url = "/api/menu/menuSelect";
        const data = {
            usrId : usrId,
        }
        const qsData = qs.stringify(data);
        await instance.post(url,qsData)
            .then(res => {
                runInAction(() => {
                    this.leftMenus = res.data.map(data => {
                        return new LeftMenu(data.menuName, "button" + data.menuIdx, "menu" + data.menuIdx, false, null, data.menuIdx, data.menuUpIdx)
                    });
                });
                sessionStorage.setItem("leftMenus", JSON.stringify(res.data));
            }).catch(error => {
                console.error(error);
            })
    }

    reRenderLing = () => {
        let tempLeftMenus = sessionStorage.getItem("leftMenus");
        if(tempLeftMenus)
        {
            try {
                tempLeftMenus = JSON.parse(tempLeftMenus);
                this.leftMenus = tempLeftMenus.map( data => {
                    return new LeftMenu(data.menuName,"button"+data.menuIdx,"menu"+data.menuIdx,false,null, data.menuIdx, data.menuUpIdx)
                });
            } catch (e) {
                console.error(e);
            }
        }
    }
}

export default MainLeftRootMenuStore;