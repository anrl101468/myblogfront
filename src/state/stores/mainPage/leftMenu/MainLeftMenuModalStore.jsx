import {action, makeObservable, observable} from "mobx";
import PropTypes from "prop-types";

export class MainLeftMenuModalStore
{
    rootStore;

    leftMenuAddModalOpen = false;

    leftMenuModifyModalOpen = false;

    addButtonName='';

    modifyButtonName = '';


    menuAdd = () =>{};

    constructor(root) {
        this.rootStore = root;

        makeObservable(this,{
            leftMenuAddModalOpen : observable,
            leftMenuModifyModalOpen : observable,
            menuAdd : observable,
            modifyButtonName : observable,
            addButtonName : observable,
            handleModalOpen : action,
            handleModalClose : action,
            buttonNameClear : action,
            modalButtonNameOnChange : action,
            modalAddMenuChange : action,
            handleModifyModalOpen : action,
            handleModifyModalClose : action,
            mopdalModifyButtonOnChange : action,
        })
    }

    modalAddMenuChange = (method) =>
    {
        this.menuAdd = () =>
        {
            method();
            this.addButtonName = '';
            this.modifyButtonName = '';
            this.leftMenuAddModalOpen = false;
            this.leftMenuModifyModalOpen = false;
        }
    }

    modalButtonNameOnChange = (event) =>
    {
        this.addButtonName = event.currentTarget.value;
    }

    mopdalModifyButtonOnChange = (event) =>
    {
        this.modifyButtonName = event.currentTarget.value;
    }

    handleModalOpen = () =>
    {
        this.leftMenuAddModalOpen = true;
    }

    handleModalClose = () =>
    {
        this.leftMenuAddModalOpen = false;
    }

    handleModifyModalOpen = (buttonName) =>
    {
        this.leftMenuModifyModalOpen = true;
        this.modifyButtonName = buttonName;
    }

    handleModifyModalClose = () =>
    {
        this.leftMenuModifyModalOpen = false;
        this.modifyButtonName ='';
    }

    buttonNameClear = (buttonName) =>
    {
        this.addButtonName = buttonName;
    }

}

MainLeftMenuModalStore.propTypes =
{
    buttonName:PropTypes.string.isRequired,
    leftMenuAddModalOpen:PropTypes.bool.isRequired
}
