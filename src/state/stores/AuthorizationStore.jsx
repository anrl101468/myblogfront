import {action, makeObservable, observable} from "mobx";

export class AuthorizationStore
{
    rootStore;

    authorization = null;
    constructor(root) {
        this.rootStore = root;
        if(sessionStorage.getItem("Authorization") != null)
        {
            this.authorization = sessionStorage.getItem("Authorization");
        }

        makeObservable(this,{
            authorization:observable,
            setAuthorization:action,
            getAuthorization:action,
        })
    }

    setAuthorization(authorization)
    {
        this.authorization = authorization;
        if(authorization != null)
        {
            sessionStorage.setItem("Authorization",authorization);
        }
        else {
            sessionStorage.removeItem("Authorization");
        }
    }

    getAuthorization()
    {
        return this.authorization;
    }
}