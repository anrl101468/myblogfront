import {Box, Typography} from "@mui/material";
import {useStores} from "../../state/context/Context";

export const JoinComplete = () =>
{
    const {usrStore} = useStores();
    return (
       <Box>
           <Typography>{usrStore.usr.usrName} 님 가입완료되었습니다.</Typography>
       </Box>
    )
}