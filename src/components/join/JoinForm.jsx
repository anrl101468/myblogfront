import {Box, FormControl, IconButton, TextField} from "@mui/material";
import {PersonAdd, Send} from "@mui/icons-material";
import {observer} from "mobx-react";
import {useStores} from "../../state/context/Context";
import { useNavigate } from "react-router-dom";

export const JoinForm = observer((props) =>
{
   const {joinStore} = useStores();
   const navigate = useNavigate();

   const idChange = (event) =>
   {
       joinStore.usrIdOnChange(event.currentTarget.value);
   }

   const nameChange = (event) =>
   {
       joinStore.usrNameOnChange(event.currentTarget.value);
   }

   const passwordChange = (event) =>
   {
       joinStore.usrPasswordOnChange(event.currentTarget.value);
   }

   const joinAxios = async () => {
       await joinStore.joinAxios(navigate);

   }

    return (
        <Box sx={{
            width:'100%',
            height:'100%',
            display:'flex',
            justifyContent:'center',
            alignItems:'center'}}
        >
            <Box sx={{position:'absolute',left:'45%',top:'10%','& > *': { margin: '1rem' }}}>
                <Box sx={{position:'fixed',left:'45%'}}>
                    <PersonAdd/>
                </Box>
                <Box sx={{position:'fixed',left:'40%',top:'20%'}}>
                    <FormControl >
                        <TextField label="ID" type='text' name='usrId' margin='dense' value={joinStore.usrId} onChange={idChange}/>
                        <TextField label="name" type='text' name='usrName' margin='dense' value={joinStore.usrName} onChange={nameChange}/>
                        <TextField label="password" type='password' name='usrPassword' margin='dense' value={joinStore.usrPassword} onChange={passwordChange}/>
                        <IconButton type="submit" onClick={joinAxios}>
                            <Send /> 가입하기
                        </IconButton>
                    </FormControl>
                </Box>
            </Box>
        </Box>
    );
});