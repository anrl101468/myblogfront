import {Box} from "@mui/material";
import {Route, Routes} from "react-router-dom";
import LoginForm from "../login/LoginForm";
import React from "react";
import Blog from "../menu/Blog";
import {JoinForm} from "../join/JoinForm";
import {JoinComplete} from "../join/JoinComplete";
import {observer} from "mobx-react";
import Main from "../menu/Main";

const MainCenterBox = observer(() =>
{
    return (
        <Box
            sx={{
                flex:1,
                minHeight:'100%',
                border:'black',
                width:'calc(100vw - 200px)' ,
                ml:'2em',
                mt:'2em',
                mr:'2em'
            }}
        >
            <Routes>
                <Route exact path='/' element={<Main/>} />
                <Route path='/:usrId' element={<Main/>} />
                <Route path='/subMenu/:menuIndex/:subMenuIndex' element={<Blog />}/>
                <Route path='/loginPage' element={<LoginForm/>} />
                <Route path='/joinPage' element={<JoinForm/>} />
                <Route path='/joinComplete' element={<JoinComplete/>} />
            </Routes>
        </Box>
    )
});
export default MainCenterBox;