import {Box, IconButton, Modal, TextField, Typography} from "@mui/material";
import {Send} from "@mui/icons-material";
import React from "react";
import {useStores} from "../../../state/context/Context";
import {observer} from "mobx-react";

export const LeftBoxAddModal = observer((props) =>
{
    const {mainLeftMenuModalStore} = useStores();
    return (
        <Modal
            open={mainLeftMenuModalStore.leftMenuAddModalOpen}
            onClose={props.onClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={props.sx}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    메뉴 추가
                </Typography>
                <TextField name='buttonName' onChange={props.onChange} value={mainLeftMenuModalStore.addButtonName}/>
                <IconButton type="submit" onClick={mainLeftMenuModalStore.menuAdd}>
                    <Send /> 만들기
                </IconButton>
            </Box>
        </Modal>
    )
});