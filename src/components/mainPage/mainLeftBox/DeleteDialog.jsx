import {observer} from "mobx-react";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import {Button} from "@mui/material";
import Dialog from "@mui/material/Dialog";
import React from "react";
import {useStores} from "../../../state/context/Context";

export const DeleteDialog = observer((props) =>
{
    const {alertDialogStore} = useStores();

    return (
        <Dialog
            open={alertDialogStore.open}
            onClose={props.onClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                {"메뉴삭제"}
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    삭제하시겠습니까? 되돌릴수 없습니다.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.agreeClick}>동의</Button>
                <Button onClick={props.cancleClick} autoFocus>
                    취소
                </Button>
            </DialogActions>
        </Dialog>
    )
})