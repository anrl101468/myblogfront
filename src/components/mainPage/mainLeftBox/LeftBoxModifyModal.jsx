import {Box, IconButton, Modal, TextField, Typography} from "@mui/material";
import {Send} from "@mui/icons-material";
import React from "react";
import {useStores} from "../../../state/context/Context";
import {observer} from "mobx-react";

export const LeftBoxModifyModal = observer((props) =>
{
    const {mainLeftMenuModalStore} = useStores();
    return (
        <Modal
            open={mainLeftMenuModalStore.leftMenuModifyModalOpen}
            onClose={props.onClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={props.sx}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    메뉴 수정
                </Typography>
                <TextField name='buttonName' onChange={props.onChange} value={mainLeftMenuModalStore.modifyButtonName}/>
                <IconButton type="submit" onClick={mainLeftMenuModalStore.menuAdd}>
                    <Send />
                </IconButton>
            </Box>
        </Modal>
    )
});