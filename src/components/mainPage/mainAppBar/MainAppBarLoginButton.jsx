import React from "react";
import {Link} from "react-router-dom";


const MainAppBarLoginButton = (props) =>
{
    return (
            <Link
                to={props.targetUrl}
                onClick={props.onClick}
                style={{color:'white', marginLeft:'10px', textDecorationLine:'none'}}
            >
                {props.title}
            </Link>
    );
};

export default MainAppBarLoginButton;