import {Typography} from "@mui/material";
import {useStores} from "../../../state/context/Context";
import {observer} from "mobx-react";

const MainAppBarMenuButton =observer( () =>
{
    const {usrStore} = useStores();

    const usr = usrStore.usr;
    return (
            <Typography style={{fontSize:'auto'}}>
                { usr ? (usr.usrName)+"님의 블로그입니다." : "자신만의 블로그를 만들어보세요"}
            </Typography>
    );
});

export default MainAppBarMenuButton;