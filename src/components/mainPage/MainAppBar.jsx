import {AppBar, IconButton, Toolbar} from "@mui/material";
import MainAppBarLoginButton from "./mainAppBar/MainAppBarLoginButton";
import MainAppBarMenuButton from "./mainAppBar/MainAppBarMenuButton";
import {observer} from "mobx-react";
import {Link, useNavigate} from "react-router-dom";
import MyLogo from "../../myStory.jpg"
import {useStores} from "../../state/context/Context";

const MainAppBar = observer( () => {

    const {authorizationStore,loginStore} = useStores();

    const navigate = useNavigate();
    const logoutClick = () =>
    {
        loginStore.logout(navigate);
    }

    return (
        <AppBar color='secondary' sx={{maxHeight:'48px',display: 'flex', justifyContent: 'space-between', py: 1,padding:0}} position='fixed'>
            <Toolbar variant='dense'>
                <IconButton>
                    <Link to='/'>
                        <img alt='logo' src={MyLogo} style={{height:'36px',width:'36px'}}/>
                    </Link>
                </IconButton>
                <Toolbar style={{marginLeft:'auto',marginRight:'auto'}}>
                    <MainAppBarMenuButton/>
                </Toolbar>
                <Toolbar>
                    {
                        authorizationStore.authorization != null?
                            (
                                <MainAppBarLoginButton title='logout' onClick={logoutClick}/>
                            ) :
                            (
                                <>
                                <MainAppBarLoginButton title='join' targetUrl= '/joinPage'/>
                                <MainAppBarLoginButton title='login' targetUrl = '/loginPage'/>
                                </>
                            )
                    }
                </Toolbar>
            </Toolbar>
        </AppBar>
    );

});

export default MainAppBar;