import {Box, Button, IconButton, Menu, MenuItem, Switch} from "@mui/material";
import React from "react";
import {useStores} from "../../state/context/Context";
import {observer} from "mobx-react";
import {Link} from "react-router-dom";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import ClearIcon from '@mui/icons-material/Clear';
import {LeftBoxAddModal} from "./mainLeftBox/LeftBoxAddModal";
import {DeleteDialog} from "./mainLeftBox/DeleteDialog";
import UpdateIcon from '@mui/icons-material/Update';
import {LeftBoxModifyModal} from "./mainLeftBox/LeftBoxModifyModal";

const MainLeftMenuBox = observer(() => {

    const {mainLeftRootMenuStore,mainLeftMenuModalStore,alertDialogStore,usrStore} = useStores();

    const buttonClick = (event, index) => {
        mainLeftRootMenuStore.handleOpen(event, index);
    }

    const menuClick = (event, index) => {
        mainLeftRootMenuStore.handleClose(event, index);
    }

    const menuAdd = (event) =>
    {
        mainLeftRootMenuStore.createLeftMenu(mainLeftMenuModalStore.addButtonName);
    }

    const menuSubAdd = (index) =>
    {
        mainLeftRootMenuStore.createSubMenu(index,mainLeftMenuModalStore.addButtonName);
    }

    const menuModify = (menuIdx) =>
    {
        mainLeftRootMenuStore.modifyLeftMenu(mainLeftMenuModalStore.modifyButtonName,menuIdx);
    }
    const menuDel = (index) =>
    {
        mainLeftRootMenuStore.deleteLeftMenu(index);
    }

    const subMenuDel = (index) =>
    {

        mainLeftRootMenuStore.deleteSubMenu(index);
    }
    const modalAddClose = () =>
    {
        mainLeftMenuModalStore.buttonNameClear('');
        mainLeftMenuModalStore.handleModalClose();
    }

    const modalAddOpen = (event,index,menuIdx) =>
    {
        if(event.currentTarget.name.toString() === 'mainMenu')
        {
            mainLeftMenuModalStore.modalAddMenuChange(menuAdd);
            mainLeftMenuModalStore.handleModalOpen();
        }
        else
        {
            mainLeftMenuModalStore.modalAddMenuChange( () => menuSubAdd(index));
            mainLeftMenuModalStore.handleModalOpen();
        }
    }

    const modalModifyClose = () =>
    {
        mainLeftMenuModalStore.handleModifyModalClose();
    }

    const modalModifyOpen = (menuIdx,buttonName) =>
    {
        mainLeftMenuModalStore.modalAddMenuChange( () => menuModify(menuIdx));
        mainLeftMenuModalStore.handleModifyModalOpen(buttonName);
    }

    const modalButtonNameChange = (event) =>
    {
        mainLeftMenuModalStore.modalButtonNameOnChange(event);
    }

    const modalModifyButtonNameChange = (event) =>
    {
        mainLeftMenuModalStore.mopdalModifyButtonOnChange(event);
    }

    const dialogOpen = (event,index) =>
    {
        alertDialogStore.setTarget(event.currentTarget.name.toString());
        alertDialogStore.setIndex(index);
        alertDialogStore.handleClickOpen();
    }

    const dialogClose = () =>
    {
        alertDialogStore.handleClose();
    }

    const dialogDeleteAgree = () =>
    {
        if(alertDialogStore.target === "mainMenu")
        {
            menuDel(alertDialogStore.index);
        }
        else if(alertDialogStore.target === "subMenu")
        {
            subMenuDel(alertDialogStore.index);
        }
        alertDialogStore.handleClose();
    }

    const modalStyle = {
        position: 'absolute',
        display: 'inline-grid',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    return (
        <Box
            sx={{
                width: '10%',
                textAlign: 'center',
                borderRight: '1px dashed darkorchid',
                minHeight: '100%'
                }}
        >
            <Box>
                {usrStore.usr != null && (
                    <Switch checked={mainLeftRootMenuStore.showIcon} onChange={mainLeftRootMenuStore.showIconChange} />
                )
                }
                {mainLeftRootMenuStore.leftMenus.map(
                        (x,index) => (
                            <Box key={index} sx={{display:'flex'}}>
                                <Box sx={{width:'-webkit-fill-available'}}>
                                    <Button
                                        id={x.buttonId}
                                        aria-controls={x.open ? x.menuId : undefined}
                                        aria-expanded={x.open ? true : undefined}
                                        onClick={(event) => buttonClick(event,index)}
                                        index={index}
                                        sx={{
                                            mt:'10px',
                                            borderBottom: '1px dashed silver',
                                            color: 'darkorchid'
                                        }}
                                    >
                                        {x.buttonName}
                                    </Button>
                                    <Menu
                                        open={x.open}
                                        anchorEl={x.anchorEl}
                                        onClose={(event) => menuClick(event,index)}
                                        anchorOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                    >
                                        {
                                            x.subMenus.map(
                                                (s,i) =>(
                                                <Box sx={{display:'flex'}} key={i}>
                                                    <Link to={'/subMenu/'+index+'/'+i} style={{textDecorationLine : 'none' ,color:'black'}} key={index}>
                                                        <MenuItem onClick={(event) => menuClick(event,index)}>
                                                            {s.subMenuName}
                                                        </MenuItem>
                                                    </Link>
                                                    {
                                                        mainLeftRootMenuStore.showIcon && (
                                                            <>
                                                            <IconButton name='mainMenu' onClick={ (event) =>  modalModifyOpen(s.menuIdx,s.subMenuName)}>
                                                                <UpdateIcon/>
                                                            </IconButton>
                                                            <IconButton name='subMenu' onClick={ (event) => dialogOpen(event,i)}>
                                                                <ClearIcon/>
                                                            </IconButton>
                                                            </>
                                                        )
                                                    }
                                                </Box>
                                                ))
                                        }
                                        {
                                            mainLeftRootMenuStore.showIcon && (
                                                <IconButton name='subMenu' onClick={ (event) => modalAddOpen(event,index,x.menuIdx)}>
                                                    <AddCircleOutlineIcon/>
                                                </IconButton>
                                            )
                                        }
                                    </Menu>
                                </Box>
                                <Box sx={{display:'flex'}}>
                                    {
                                        mainLeftRootMenuStore.showIcon && (
                                            <>
                                                <IconButton name='mainMenu' sx={{mt:'8px'}} onClick={ (event) =>  modalModifyOpen(x.menuIdx,x.buttonName)}>
                                                    <UpdateIcon/>
                                                </IconButton>
                                                <IconButton name='mainMenu' sx={{mt:'8px'}} onClick={(event) => dialogOpen(event,index)}>
                                                    <ClearIcon/>
                                                </IconButton>
                                            </>
                                        )
                                    }
                                </Box>
                            </Box>
                        )
                    )
                }
            </Box>
            {
                usrStore.usr != null && mainLeftRootMenuStore.showIcon?
                (<IconButton name='mainMenu' sx={{mt:5}} onClick={ (event) => modalAddOpen(event)}>
                    <AddCircleOutlineIcon/>
                </IconButton>)
                :
                (<Box></Box>)
            }

            <LeftBoxAddModal
                onClose={modalAddClose}
                sx={modalStyle}
                onChange={modalButtonNameChange}
            />

            <LeftBoxModifyModal
                sx={modalStyle}
                onClose={modalModifyClose}
                onChange={modalModifyButtonNameChange}
            />

            <DeleteDialog
                onClose={dialogClose}
                agreeClick={dialogDeleteAgree}
                cancleClick={dialogClose}
            />
        </Box>
    );
});


export default MainLeftMenuBox;