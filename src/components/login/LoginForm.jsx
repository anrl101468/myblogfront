import {Box, FormControl, IconButton, TextField, Typography} from "@mui/material";
import {PersonAdd, Send} from "@mui/icons-material";
import {useStores} from "../../state/context/Context";
import {useNavigate} from "react-router-dom";
import {observer} from "mobx-react";

const LoginForm = observer(() =>
{
    const {loginStore} = useStores();

    const navigate = useNavigate();
    const usrIdOnChange = (event) =>
    {
        loginStore.usrIdChange(event.currentTarget.value);
    }

    const usrPasswordOnChange = (event) =>
    {
        loginStore.usrPasswordChange(event.currentTarget.value);
    }

    const loginOnClick = () =>
    {
        loginStore.loginClick(navigate);
    }

    return (
        <Box sx={{
            width:'100%',
            height:'100%',
            display:'flex',
            justifyContent:'center',
            alignItems:'center'}}
        >
            <Box sx={{position:'absolute',left:'45%',top:'10%','& > *': { margin: '1rem' }}}>
                <Box sx={{position:'fixed',left:'45%'}}>
                    <PersonAdd/>
                </Box>
                <Box sx={{position:'fixed',left:'40%',top:'20%','& > & ': { marginTop:'1rem'}}}>
                    <FormControl>
                        <TextField label="ID" type='text' name='usrId' margin='dense' onChange={usrIdOnChange} value={loginStore.usrId} />
                        <TextField label="password" type='password' name='usrPassword' margin='dense' onChange={usrPasswordOnChange} value={loginStore.usrPassword}/>
                        <IconButton type="submit" onClick={loginOnClick}>
                            <Send /> 로그인
                        </IconButton>
                    </FormControl>
                </Box>
                <Box sx={{position:'fixed' , left:'35%',top:'45%'}}>
                    <Typography sx={{color: 'red'}} >{loginStore.message}</Typography>
                </Box>
            </Box>
        </Box>
    )
});

export default LoginForm;
