import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {useStores} from "../../state/context/Context";
import {observer} from "mobx-react";
import {Collapse, IconButton, TableFooter, TablePagination, TextField} from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import SettingsIcon from '@mui/icons-material/Settings';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 15,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const ListTable = observer(() => {
    const {blogStore, postPagingStore, mainLeftRootMenuStore,postStore} = useStores();

    const expandIconClick = (uplIdx) =>
    {
        const index = blogStore.posts.findIndex( (post) => post.uplIdx === uplIdx);
        blogStore.expandIconClick(index);
    };

    const rowsChange = (event) =>
    {
        postPagingStore.rowsPerPageChange(parseInt(event.target.value));
    }

    const pageChange = (event,newPage) =>
    {
        postPagingStore.pageChange(newPage);
    }

    const postWrite = (uplIdx) =>
    {
        const index = blogStore.posts.findIndex( (post) => post.uplIdx === uplIdx)
        const title = blogStore.posts[index].uplTitle;
        const content = blogStore.posts[index].uplContent;
        postStore.titleChange(title);
        postStore.contentChange(content);
        postStore.typeChange("modify");
        postStore.uplIdxChange(uplIdx);
        blogStore.PostWriteModalOpen();
    }

    return (
        <TableContainer component={Paper}>
            <Table aria-label="customized table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell aiign="center" sx={{width:'5%'}}></StyledTableCell>
                        <StyledTableCell align="center" sx={{width:'5%'}}>No</StyledTableCell>
                        <StyledTableCell align="center" sx={{width:'74%'}}>제목</StyledTableCell>
                        <StyledTableCell align="center" sx={{width:'16%'}}>작성일</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    { (postPagingStore.rowsPerPage > 0 ?
                        blogStore.posts.slice(postPagingStore.page * postPagingStore.rowsPerPage, postPagingStore.page * postPagingStore.rowsPerPage + postPagingStore.rowsPerPage)
                        :
                        blogStore.posts).map((row,index) => (
                        <React.Fragment key={index}>
                            <StyledTableRow>
                                <StyledTableCell component="th" scope="row">
                                    <IconButton
                                        aria-label="expand row"
                                        size="small"
                                        onClick={() => expandIconClick(row.uplIdx)}
                                    >
                                        {row.open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                                    </IconButton>
                                </StyledTableCell>
                                <StyledTableCell component="th" scope="row" sx={{textAlign:'center'}}>
                                    {index+1}
                                </StyledTableCell>
                                <StyledTableCell align="center">{row.uplTitle}
                                    {
                                        mainLeftRootMenuStore.showIcon && (<IconButton sx={{ml:'1em'}} onClick={ () => postWrite(row.uplIdx)}> <SettingsIcon/> </IconButton>)
                                    }
                                </StyledTableCell>
                                <StyledTableCell align="center">{row.uplRegDt}</StyledTableCell>
                            </StyledTableRow>
                            <StyledTableRow>
                                <StyledTableCell component="th" style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={4}>
                                    <Collapse in={row.open} timeout="auto" unmountOnExit>
                                        <TextField
                                            id="outlined-read-only-input"
                                            InputProps={{
                                                readOnly: true,
                                                multiline: true,
                                            }}
                                            sx={{width:'100%'}}
                                            value={row.uplContent}
                                        />
                                    </Collapse>
                                </StyledTableCell>
                            </StyledTableRow>
                        </React.Fragment>
                    ))}
                </TableBody>
                <TableFooter>
                    <StyledTableRow>
                    <TablePagination rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                     count={blogStore.posts.length}
                                     rowsPerPage={postPagingStore.rowsPerPage}
                                     onRowsPerPageChange={rowsChange}
                                     page={postPagingStore.page}
                                     onPageChange={pageChange}
                                     colSpan={4}
                    />
                    </StyledTableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    );
});

export default ListTable;