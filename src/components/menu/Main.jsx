import {Box} from "@mui/material";
import {useParams} from "react-router-dom";
import {useStores} from "../../state/context/Context";
import {useEffect} from "react";

const Main = () => {

    const params = useParams();

    const {mainLeftRootMenuStore} = useStores();
    useEffect( () => {
        if(params.usrId)
        {
            mainLeftRootMenuStore.getMainMenuServerInfo(params.usrId);
        }
    })
    return (
        <Box>

        </Box>
    )
}

export default Main;