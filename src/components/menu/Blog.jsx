import * as React from 'react';
import {Box, Button, TextField, Typography} from "@mui/material";
import Divider from "@mui/material/Divider";
import {useParams} from "react-router-dom";
import {observer} from "mobx-react";
import {useStores} from "../../state/context/Context";
import ListTable from "./ListTable";
import {PostWriteModal} from "./PostWriteModal";

const Blog = observer(() => {
    const {blogStore} = useStores();

    const params = useParams();

    if(blogStore.menuIndex != params.menuIndex || blogStore.subMenuIndex != params.subMenuIndex)
    {
        blogStore.searchKeywordChange("");
    }
    const subMenu = blogStore.setIndex(params.menuIndex, params.subMenuIndex);


    const modalStyle = {
        position: 'absolute',
        display: 'inline-grid',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '70%',
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const postWrite =() =>
    {
        blogStore.PostWriteModalOpen();
    }

    const searchKeywordChange = (event) =>
    {
        blogStore.searchKeywordChange(event.target.value);
    }

    return (
        <React.Fragment>
            <Box
                sx={{
                    minHeight:'100%',
                    bgcolor: 'background.paper',
                    overflow:'hidden'
                    }}>
                <Box sx={{width:'100%',height:'10%', display:'flex', justifyContent:'space-between',alignItems:'center'}}>
                    <Box>
                        <Typography sx={{fontSize:'24px'}}>
                            {
                                subMenu && (subMenu.subMenuName)
                            }
                        </Typography>
                        <Divider variant="inset" sx={{ml:0}}/>
                    </Box>
                    <TextField
                        label="검색어 입력"
                        variant="outlined"
                        value={blogStore.searchKeyword}
                        onChange={searchKeywordChange}
                        sx={{ width: '300px', margin: '8px 0' }}
                    />
                    <Button variant="contained" size="medium" onClick={postWrite}>
                        글쓰기
                    </Button>
                </Box>
                <Box sx={{mb:'2em'}}>
                    <ListTable />
                </Box>
            </Box>
            <PostWriteModal sx={modalStyle}/>
        </React.Fragment>
    );
});

export default Blog;