import {Box, IconButton, Modal, TextField} from "@mui/material";
import {Send} from "@mui/icons-material";
import React from "react";
import {observer} from "mobx-react";
import {useStores} from "../../state/context/Context";

export const PostWriteModal = observer((props) =>
{
    const {blogStore,postStore} = useStores();

    const postClose = () =>
    {
        blogStore.PostWriteModalClose();
    }

    const titleChange = (event) =>
    {
        postStore.titleChange(event.currentTarget.value);
    }

    const contentChange = (event) =>
    {
        postStore.contentChange(event.currentTarget.value);
    }

    const createPost = () =>
    {
        blogStore.createPost(postStore.title,postStore.content);
    }

    const modifyPost = () =>
    {
        blogStore.modifyPost(postStore.title,postStore.content,postStore.uplIdx);
    }

    const deletePost = () =>
    {
        blogStore.deletePost(postStore.uplIdx);
    }

    return (
        <Modal
            open={blogStore.postWriteOpen}
            onClose={postClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={props.sx}>
                <TextField placeholder="제목" sx={{mb:"1em"}} onChange={titleChange} value={postStore.title}/>
                <TextField multiline={true} minRows='8' onChange={contentChange} value={postStore.content}/>
                {
                    postStore.type == 'create' ?
                        (<IconButton type="submit" onClick={createPost}>
                        <Send /> 만들기
                        </IconButton>) :
                        (
                            <Box sx={{display:'flex', justifyContent:'center'}}>
                                <IconButton type="submit" onClick={modifyPost}>
                                    <Send /> 수정
                                </IconButton>
                                <IconButton type="submit" onClick={deletePost}>
                                    <Send /> 삭제
                                </IconButton>
                            </Box>
                        )
                }
            </Box>
        </Modal>
    )
});