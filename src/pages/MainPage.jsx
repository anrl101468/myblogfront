import React from "react";
import MainAppBar from '../components/mainPage/MainAppBar'
import MainLeftMenuBox from "../components/mainPage/MainLeftMenuBox";
import MainCenterBox from "../components/mainPage/MainCenterBox";
import {Box} from "@mui/material";
const MainPage = () =>
{
    return (
        <Box sx={{position: 'relative', width:'100%', height:'100%', overflowX:'hidden'}}>
            <MainAppBar/>
            <Box
                sx={{display:'flex',flexDirection:'row',position:'relative',top:'48px', minHeight:'calc( 100% - 48px )',mb:'2em'}}
            >
                <MainLeftMenuBox/>
                <MainCenterBox/>
            </Box>
            {/*
            <Box
                sx=
                {{
                    display:'block',
                    position:'absolute',
                    top: 'calc(100vh - 52px)',
                    width:'100vw',
                    bgcolor :'green',
                    height:'calc(100vh - (100vh - 52px))'
                }}
            >
            </Box>*/}
        </Box>
    )
}

export default MainPage;